const path = require("path");

module.exports = {
  // output: "export",
  reactStrictMode: true,
  experimental: {
    scrollRestoration: true,
  },
  sassOptions: {
    includePaths: [path.join(__dirname, "assets/styles")],
    prependData: `
    @import "./global.scss";
    `,
  },
};
