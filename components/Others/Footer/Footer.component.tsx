import React from "react";
import styles from "./Footer.module.scss";
import { footerLinks } from "@/constants";
import Link from "next/link";
const Footer = () => {
  return (
    <footer className={styles.footer}>
      <div className={styles["footer__content"]}>
        <div className={styles["footer__content-section"]}>
          <img className={styles["footer__logo"]} src="./logo.svg" alt="logo" />
          <p className={styles["footer__copy-right"]}>
            Cars App <br />
            All rights reserved &copy;
          </p>
        </div>

        <div className={styles["footer__links"]}>
          {footerLinks?.map((item, index) => (
            <div key={index} className={styles["footer-link__item"]}>
              <span className={styles["footer-link__item-title"]}>{item?.title}</span>
            <div key={index} className={styles["footer-link__items"]}>

              {item?.links?.map((item, index) => (
                <Link className={styles["footer-link__item-link"]} key={index} href={item?.url}>
                  {item?.title}{" "}
                </Link>
              ))}
              </div>
            </div>
          ))}
        </div>
      </div>
    </footer>
  );
};

export default Footer;

