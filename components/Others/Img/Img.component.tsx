"use client";
import { useEffect, useState } from "react";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";
import styles from "./Img.module.scss";
import { ImgType } from "@/types";

function Img({
  path,
  headers,
  fallbackFile = "/assets/img/cover.png",
  figureStyle = {},
  onLoadEnd = () => {},
}: ImgType) {
  const [src, setSrc] = useState(null);
  const [reTry, setReTry] = useState(1);
  const [loading, setLoading] = useState(true);

  const getStaticFile = async () => {
    if (path) {
      try {
        setLoading(true);
        const response = await fetch(path, headers);
        const blob = await response.blob();
        const reader: any = new FileReader();
        reader.readAsDataURL(blob);
        reader.onload = () => {
          setSrc(reader.result);
          setLoading(false);
          onLoadEnd();
        };
      } catch (error) {
        if (reTry < 3) {
          setReTry(reTry + 1);
        } else {
          setSrc(fallbackFile);
        }
        setLoading(false);
        onLoadEnd();
        console.error("Error in getStaticFile", error);
      }
    } else {
      setSrc(fallbackFile);
      onLoadEnd();
    }
  };

  useEffect(() => {
    getStaticFile();
  }, [path, reTry]);

  return src ? (
    <img className={styles.img} src={src} alt="Loaded Image" />
  ) : (
    <figure className={styles.loading}>
      <Skeleton className={styles.loading} />
    </figure>
  );
}

export default Img;

