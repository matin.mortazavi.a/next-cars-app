import React from "react";
import Link from "next/link";
import styles from "./Navbar.module.scss";
import { Btn } from "@/components/Others";
const Navbar = () => {
  return (
    <header className={styles["navbar"]}>
      <nav className={styles["navbar__content"]}>
        <Link  href="/">
          <img className={styles["navbar__logo"]} src ="./logo.svg"/>
        </Link>
        <Btn title="Sign in" variant="contained" />
      </nav>

    </header>
  );
};

export default Navbar;

