"use client";
import React, { useMemo } from "react";
import Image from "next/image";
import styles from "./Btn.module.scss";
import { BtnType } from "@/types";
export const Btn = ({
  type,
  variant = "text",
  className,
  danger = false,
  title,
  block = false,
  color = "primary",
  onClick = () => {},
}: BtnType) => {
  const createClassName: any = useMemo(
    () =>
      [
        styles.btn,
        variant == "contained" && color == "primary" && styles["btn__contained-primary"],
        variant == "contained" && color == "secondary" && styles["btn__contained-secondary"],
        variant == "text" && color == "inherit" && styles["btn__text--inherit"],
        variant == "text" && color == "primary" && styles["btn__text-primary"],
        variant == "text" && color == "secondary" && styles["btn__text-secondary"],
        variant == "outlined" && color == "primary" && styles["btn__outlined-primary"],
        variant == "outlined" && color == "secondary" && styles["btn__outlined-secondary"],
        block && styles.btn__block,
        danger && styles.btn__danger,
        className,
      ].join(" "),
    [variant, block, danger, className, color],
  );
  return (
    <button disabled={false} className={createClassName} onClick={onClick}>
      <span >{title}</span>
    </button>
  );
};

export default Btn;

