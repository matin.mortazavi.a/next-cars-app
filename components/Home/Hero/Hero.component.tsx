"use client";
import React from "react";
import styles from "./Hero.module.scss";
import { Btn, Img } from "@/components/Others";
import Image from "next/image";
import { heroData } from "@/constants";
export const Hero = () => {
  const onBtnClick = () => {};
  return (
    <div className={styles.hero}>
      <div className={styles["hero__wrapper"]}>
        <h1 className={styles["hero__title"]}>{heroData?.title}</h1>
        <p className={styles["hero__subtitle"]}>{heroData?.subTitle}</p>

        <Btn variant="contained" title="Explore cars" onClick={onBtnClick} />
      </div>
      <div className={styles["hero__img-container"]}>
        <figure className={styles["hero__img-figure"]}>
          <img className={styles["hero__img"]} src="/hero.png" alt="logo" />
        </figure>
        <img className={styles["hero__img-overlay"]} src="/hero-bg.png" alt="logo" />
      </div>
    </div>
  );
};

export default Hero;
