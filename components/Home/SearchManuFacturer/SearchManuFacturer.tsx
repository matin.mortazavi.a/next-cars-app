"use client";
import { Combobox, Transition } from "@headlessui/react";
import React, { useState, Fragment } from "react";
import styles from "./SearchManuFacturer.module.scss";
import { SearchMenuFacturerType } from "@/types";
import { manufacturers } from "@/constants";
import { CheckIcon } from "@heroicons/react/20/solid";

const SearchManuFacturer = ({ menuFacturer, setMenuFacturer }: SearchMenuFacturerType) => {
  const [query, setQuery] = useState("");

  const filteredMenuFacturers =
    query === ""
      ? manufacturers
      : manufacturers.filter((item) =>
          item.toLocaleLowerCase().replace(/\s+/g, "").includes(query.toLowerCase().replace(/\s+/g, "")),
        );

  return (
    <div className={styles["search-menu-facturer"]}>
      <Combobox value={menuFacturer} onChange={setMenuFacturer}>
        <div className={styles["combo-box"]}>
          <Combobox.Button className={styles["combo-box__btn"]}>
            <img className={styles["combo-box__logo"]} src="./car-logo.svg" alt="logo" />
          </Combobox.Button>
          <Combobox.Input
            placeholder="VolksWagen"
            displayValue={(menuFacturer: string) => menuFacturer}
            className={styles["combo-box__input"]}
            onChange={(e) => setQuery(e.target.value)}
          />
        </div>
        <Transition
          as={Fragment}
          leave="transition ease-in duration-100"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
          afterLeave={() => setQuery("")}
        >
          <Combobox.Options>
            {!filteredMenuFacturers.length && query !== "" ? (
              <Combobox.Option value={query} className={styles["search-menufacturer__option"]}>
                Create "{query}"
              </Combobox.Option>
            ) : (
              filteredMenuFacturers?.map((item, index) => (
                <Combobox.Option
                  key={index}
                  value={item}
                  className={({ active }) =>
                    `relative py-2 
                  ${active ? "bg-primary-blue text-white" : "text-gray-900"}`
                  }
                >
                  {({ active, selected }) => (
                    <div className="flex px-4">
                      {selected && <CheckIcon width={20} height={20} />}

                      <span className={`${active ? "text-white ml-2  " : "bg-white ml-2 text-black"}`}>
                        {item}
                      </span>
                    </div>
                  )}
                </Combobox.Option>
              ))
            )}
          </Combobox.Options>
        </Transition>
      </Combobox>
    </div>
  );
};

export default SearchManuFacturer;

