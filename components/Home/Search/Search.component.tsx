"use client";
import React, { FormEventHandler, useState } from "react";
import styles from "./Search.module.scss";
import { SearchManuFacturer } from "@/components/Home";
const Search = () => {
  const [menuFacturer, setMenuFacturer] = useState("");
  const handleSearch: FormEventHandler = () => {};
  return (
    <form className={styles["search__form"]}  onSubmit={handleSearch}>
      <div className={styles["search__item"]}>
        <SearchManuFacturer setMenuFacturer={setMenuFacturer} />
      </div>
    </form>
  );
};

export default Search;

