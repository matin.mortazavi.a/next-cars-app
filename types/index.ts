import { MouseEventHandler, SetStateAction } from "react";

export interface BtnType {
  type?: "submit" | "button";
  title: string;
  variant?: string;
  color?: string;
  block?: any;
  danger?: any;
  className?: string;
  onClick?: MouseEventHandler<HTMLButtonElement>;
}
export interface ImgType {
  path: string;
  headers?: {};
  fallbackFile?: SetStateAction<any>;
  figureStyle?: {};
  onLoadEnd?: any;
}

export interface FilterType {
  title: string;
}
export interface SearchType {
  title: string;
}

export interface SearchMenuFacturerType {
  menuFacturer: string;
  setMenuFacturer: (menuFacturer: string) => void;
}

