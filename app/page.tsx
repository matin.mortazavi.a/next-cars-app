import { cars } from "@/constants";
import styles from "./page.module.scss";
import { Hero, Search, Filter } from "@/components/Home";

export default function Home() {
  return (
    <main>
      <Hero />

      <div className={styles["cars"]} id="discover">
        <div className={styles["cars__text-wrapper"]}>
          <h2 className={styles["cars__title"]}>{cars?.title}</h2>
          <p className={styles["cars__subtitle"]}>{cars?.subTitle}</p>
        </div>
        <div className={styles["cars__filters"]}>
          <Search />

          <div className={styles["cars__filter-container"]}>
            <Filter title="fuel" />
            <Filter title="year" />
          </div>
        </div>
      </div>
    </main>
  );
}
